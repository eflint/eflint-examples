# DIPG regulatory document example specification

Based on the DIPG Regulatory document V2 found [here](https://dipgregistry.eu/Content/files/2018-10-10SIOPEDIPGRegistry-RegulatoryDocument_v%202.0_final.pdf)

Use case presented in: 

```
L. Thomas van Binsbergen, Milen G. Kebede, Joshua Baugh, Tom van Engers, Dannis G. van Vuurden,
Dynamic generation of access control policies from social policies,
Procedia Computer Science, Volume 198, 2022, Pages 140-147.
```
https://doi.org/10.1016/j.procs.2021.12.221
