# School Regulations v0.1

## Definitions

(Article 1) A teacher is any member of staff _owning_ one of the courses provided by the school.

(Article 2) A learner is any person _enrolled_ into one of the courses provided by the school.

## Regulations

(Article 3) The teacher of a course can set assignments to all learners enrolled in the course provided the duedate of the assignment is at least 24 hours beyond the date the assignment is set.

> A conditional power. Not only must the setter of the assignment own a course (i.e. be a teacher), but also must the assignees be learners in the course owned by the teacher at the time the assignment is set. A formal representation of this article therefore requires the ability to maintain relations between multiple instances of concepts, as multiple courses can exist, each with a different teacher and with multiple learners enrolled. Implicitly, the assignment must have a duedate.

(Article 4) The duedate of any assignment must be at 8am of a school day.

> A fairly arbitrary rule that permits the simplification of the representation of due dates, e.g. by using a day rather than a time on a date, by assuming all school days start at 8am the earliest.

(Article 5) All learners must hand in _completed assignments_ before the duedate of the assignment. Repeated failure to do so can result in _suspension of the learner_

> This article contains a duty on learners as well as a power for schools. Interesting aspects are that the school (or schoolboard) concept itself is not explicitly mentioned in these regulations. Moreover, there is a large degree of freedom for the school to decide how many violations of Article (5) warrents suspension.

(Article 6) An assignment is considered completed if so recognized by the teacher setting the assignment and if it is clearly marked with all (co-)authors contributing to the assignment.

> According to this article, the teacher has the power to determine whether an assignment is completed, e.g. is considered a sufficient attempt to produce an essay or report on the given topic.
> Here we also encounter the issue of identification. Authors must be identified based on their signatures. Moreover, properties of the identified authors must be verified in order to enforce Article (7), i.e. whether they are a legal parent.

(Article 7) Only the learner to which an assignment has been set and their legal parents -- as defined by (Article 1) of the Parentship Reulations v0.1 -- are considered valid (co-)authors of an assignment.

> A conditional power that requires external verification of the "legal parent" property. Note also the reference to another source of norms.
