# Parentship laws v0.1

## Definitions

(Article 1) A natural person is a _legal parent_ of another natural person if:

1. the first person is a natural parent of the second person
2. the first person is an adoptive parent of the second person

> The fact that one person is the legal parent of another can be derived from other facts. Ideally, the chosen language to represent norms can allow such facts to be computed automatically based on the truth of the facts from which they are derived.

(Article 2) A natural person is a natural parent of another natural person if:

1. if the first person has given birth to the second person
2. if the first person has recognized the second person as their child, is the only person to have done so and the birth parent -- as defined in (2.1) -- has completed and signed form PAR:NM:01, giving permission to the first person for the recognition of the child.

> A more complicated derived fact, requiring the verification of a form, the verification of the signature on the form and the authority of the person identified by the signature, i.e. that the signee is indeed the birth parent of the child. Each of these factors is an opportunity for fraud.

(Article 3) A natural person is an adoptive parent of another natural person if ...

> Further specification of this article delivers no further complexity to the example