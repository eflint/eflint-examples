# Template Caretaker Contract v0.1

* In what follows, "caretaker" refers to PERSON1

> Here `PERSON1` is a parameter of the template. That is, when the template is instantiated, `PERSON1` is replaced with the name of the first signee of the (instantiated) contract.

* In what follows, "child" refers to PERSON2

> The child is the second signee of the contract.

(Article 1) The child promises to obey any prohibitions and obligations set to them by the caretaker under the principle "my roof, my rules".

> In other words, the caretaker is very powerful, as they can produce arbitrary rules for the child to obey. However, the only enforcement mechanism this contract provides is (Article 2).

(Article 2) Upon violation of any of the prohibitions and obligations set by invoking (Article 1), the caretaker has the power to _ground_ the child for at most 4 weeks.

> There is freedom for the caretaker to choose the exact amount of weeks the child is being grounded, e.g. based on the severity of the violation.

(Article 3) The caretaker promises to pay the child a weekly allowance of INITIAL-ALLOWANCE of which the first payment is to occur within 1 day relative to START-DATE and subsequent payments occur exactly 7 days after the previous payment.

> Allowance is introduced to compensate the power granted to the caretaker by Articles (1) and (2). The initial allowance is one of the parameters of the contract, decided by the two parties before the contract is signed. The start date of the contract is another parameter.

(Article 4) The child has the power to demand help with helpwork, placing a duty on the caretaker to help before the duedate of the homework.

> In addition to Article (3), the child can demand help with homework in order to meet their school requirements

(Article 5) The caretaker has the power to refuse helping with homework if the duedate of the homework is less than 24 hours away from the date the demand -- as defined in (Article 4) -- has been placed.

> Unreasonable requests can be ignored by this power. Reasonable requests can be ignored as well (although breaking this contract), for which there is no direct enforcement. The (indirect) incentive for the caretaker to help is in their goal of getting the child through school. Repeated failure to complete homework can result in suspension after all.

(Article 6) The child can demand compensation in the form of a 20% increase of their allowance in case the caretaker has failed to provide help with homework before the duedate of the homework

> A more direct incentive is the cost to the caretaker of failing to comply with the duty to help