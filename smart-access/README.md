# Examples from Smart Access paper 

Formalisation of the two scenarios in the original paper on Smart Access.

* Scenario based on patient consent (Section IV.A) can be found in `case_patient_consent.eflint`
* Scenario based on acute emergency (Section IV.B) can be found in `case_emergency.eflint`

```
SmartAccess: Attribute-Based Access Control System for Medical Records Based on Smart Contracts,
M. Tuler De Oliveira, L. H. A. Reis, Y. Verginadis, D. M. F. Mattos and S. D. Olabarriaga, 
in IEEE Access, vol. 10, pp. 117836-117854, 2022.
```

[https://doi.org/10.1109/ACCESS.2022.3217201](https://doi.org/10.1109/ACCESS.2022.3217201)

